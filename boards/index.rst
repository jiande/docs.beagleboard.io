.. _boards:

Boards
######

:ref:`BeagleBone <beaglebone-all-home>` is a family of ARM-based, Linux-capable boards intended to be
bare-bones, with a balance of features to enable rapid prototyping and provide a solid
reference for building end products.

:ref:`pocketbeagle-home` boards are ultra-tiny ARM-based, Linux-capable boards intended to be
very low cost, with minimal features suitable for beginners and attractive to professionals
looking for a more minimal starting point.

BeagleBone and PocketBeagle :ref:`capes` are add-on boards for BeagleBone and PocketBeagle boards.

:ref:`beagleconnect-home` boards are ARM microcontroller-based, Zephyr-capable boards meant
to act as ultra low cost smart peripherals to their Linux-capable counterparts, with connectivity
options that enable almost endless sensing and actuation expansion.

:ref:`BeagleBoard <beagleboard-boards-home>` is a family of ARM-based, Linux-capable boards where this project
started.

.. toctree::
   :maxdepth: 1

   /boards/beaglebone/index
   /boards/pocketbeagle/original/index
   /boards/capes/index
   /boards/beagleconnect/index
   /boards/beagleboard/index

