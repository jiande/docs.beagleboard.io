.. _support-information:

Support Information
####################

All support for this design is through BeagleBoard.org community at: link: `BeagleBoard.org forum <https://forum.beagleboard.org/>`_ .


.. _hardware-design:

Hardware Design
------------------

You can find all BeagleBone AI-64 hardware files `here <https://git.beagleboard.org/beagleboard/beaglebone-ai-64/-/tree/master/hw>`_ .


.. _software-updates:

Software Updates
-------------------------------------

You can download and flash the supported image onto your BeagleBone AI-64 from `this <https://debian.beagle.cc/images/bbai64-emmc-flasher-debian-11.2-xfce-arm64-2022-01-14-8gb.img.xz>`_ source.

To see what SW revision is loaded into the eMMC check `/etc/dogtag`.
It should look something like as shown below,

```
root@BeagleBone:~# cat /etc/dogtag
BeagleBoard.org Debian Bullseye Xfce Image 2022-01-14
```

.. _rma-support:

RMA Support
-------------------------------------

If you feel your board is defective or has issues, request an Return Merchandise Application (RMA) by filling out the form at http://beagleboard.org/support/rma . You will need the serial number and revision of the board. The serial numbers and revisions keep moving. Different boards can have different locations depending on when they were made. The following figures show the three locations of the serial and revision number.

.. _trouble-shooting-video-output-issues:

Troubleshooting video output issues
-------------------------------------

.. warning:: 

   When connecting to an HDMI monitor, make sure your miniDP adapter is *active*. A *passive* adapter will not work. See :ref:`display-adaptors-figure`.


.. _getting-help:

Getting Help
*************

If you need some up to date troubleshooting techniques, you can post your queries on link: `BeagleBoard.org forum <https://forum.beagleboard.org/>`_
