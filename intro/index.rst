.. _introduction:

Introduction
#############

Welcome to the BeagleBoard documentation project. If you are looking for help with your Beagle
open-hardware development platform, you've found the right place!

Please check out our :ref:`support` page` to find out how to get started, resolve issues,
and engage the developer community.

Don't forget that this is an open-source project! Your contributions are welcome. Learn about how
to contribute to the BeagleBoard documentation project and any of the many open-source Beagle
projects on-going on our :ref:`contribution` page.

.. toctree::
   :maxdepth: 2

   /intro/support/index
   /intro/bone101/index
   /intro/contribution/index

